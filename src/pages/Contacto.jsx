import React, { useRef } from "react";

import ContactoForm from "../components/Login/Contacto-form";

const Contacto = () => {
  const ref = useRef(null);
  const contacto = (values) => {
    console.log(values);
  };

  let ds = false;
  if (ref.current) {
    ds = ref.current.isSubmitting;
  }
  return (
    <div className="container">
      <div className="row">
        <div className="col-6"></div>
        <div className="col-6">
          <ContactoForm onSubmit={contacto} innerRef={ref} />
          <button
            className="btn btn-primary"
            onClick={() => {
              if (ref.current) {
                ref.current.submitForm();
              }
            }}
          >
            Registrar
          </button>
        </div>
      </div>
    </div>
  );
};

export default Contacto;
