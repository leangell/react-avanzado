import { Field, Form, Formik } from "formik";
const ContactoForm = ({ onSubmit, innerRef }) => {
  const inicial_data = {
    name: "",
    email: "",
    cell_phone: "",
    address: "",
    description: "",
  };

  const submit = (values, actions) => {
    actions.setSubmitting(true);
    onSubmit(values);
    actions.resetForm();
    actions.setSubmitting(false);
  };

  return (
    <Formik initialValues={inicial_data} onSubmit={submit} innerRef={innerRef}>
      {({}) => {
        return (
          <Form>
            <div className="form-group">
              <label htmlFor="name"> Usuario </label>
              <Field
                className="form-control"
                id="name"
                type="text"
                name="name"
                placeholder="Ingrese su nombre "
              />
            </div>
            <div className="form-group">
              <label htmlFor="email"> Correo electronico </label>
              <Field
                className="form-control"
                id="email"
                type="email"
                name="email"
                placeholder="Ingrese su correo "
              />
            </div>
            <div className="form-group">
              <label htmlFor="address:"> Direccion </label>
              <Field
                className="form-control"
                id="address:"
                type="text"
                name="address:"
                placeholder="Direccion de  domicilio  "
              />
            </div>
            <div className="form-group">
              <label htmlFor="cellphone"> Telefono </label>
              <Field
                className="form-control"
                id="cellphone"
                type="number"
                name="cell_phone"
                placeholder="Numero de celular o fijo "
              />
            </div>
            <div className="form-group">
              <label htmlFor="description:"> Describete </label>
              <Field
                className="form-control"
                id="description:"
                type="text"
                name="description:"
                placeholder="Descripcion "
              />
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

export default ContactoForm;
