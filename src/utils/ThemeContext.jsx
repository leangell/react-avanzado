import React, { useState } from "react";

import { Children } from "react/cjs/react.production.min";
import { createContext } from "react";

const ThemeContext = createContext();
const inicialTheme = "light";
const ThemeProvider = ({ children }) => {
  const [theme, setTheme] = useState(inicialTheme);

  const handleTheme = (e) => {
    if (e.target.value === "light") {
      setTheme("light");
    } else {
      setTheme("dark");
    }
  };

  const data = { theme, setTheme };
  return (
    <ThemeContext.Provider value={data.theme}>{children}</ThemeContext.Provider>
  );
};

export { ThemeProvider };
export default ThemeContext;
