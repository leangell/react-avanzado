import React, { useState } from "react";
const inicial_data = {
  theme: "ligth",
};

export const ThemeContext = React.createContext(inicial_data);

const { Provider, Consumer } = ThemeContext;

const ThemeProvider = ({ _theme, children }) => {
  const [theme, setTheme] = useState(_theme);
  const toggle_theme = () => {
    setTheme((c) => (c === "light" ? "dark" : "light"));
  };
  console.log(theme, toggle_theme);
  return <Provider value={{ theme, toggle_theme }}>{children}</Provider>;
};

export default ThemeProvider;
