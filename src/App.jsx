import "./App.css";

import { Route, Routes } from "react-router-dom";

import Contacto from "./pages/Contacto";
import Informacion from "./pages/Informacion";
import Login from "./pages/Login";
import Navbar from "./components/Login/Navbar";
import Portada from "./pages/Portada";
import ThemeProvider from "./utils/theme.contex";

function App() {
  return (
    <ThemeProvider _theme={"light"}>
      <div className="App">
        <div>
          <Navbar />
          <Routes>
            <Route path="/" element={<Portada />} />
            <Route path="/Login" element={<Login />} />
            <Route path="/Contacto" element={<Contacto />} />
            <Route path="/Informacion" element={<Informacion />} />
          </Routes>
        </div>
      </div>
    </ThemeProvider>
  );
}

export default App;
